from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware  # Importieren Sie die benötigte Middleware
from fastapi.openapi.utils import get_openapi
from fastapi.staticfiles import StaticFiles
from .routers import compatibilitycheck, dependencygraph, licenselibrary, datasets
from starlette.responses import FileResponse
from dotenv import load_dotenv
from pathlib import Path
import os

# Create class for the whole api and put env loading in init function
dotenv_path = Path('/.env')
load_dotenv(dotenv_path=dotenv_path)

app = FastAPI()

# CORS-Middleware-Konfiguration hinzufügen
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],  # Erlaubt Anfragen von allen Domänen
    allow_credentials=True,
    allow_methods=["*"],  # Erlaubt alle HTTP-Methoden
    allow_headers=["*"],  # Erlaubt alle Header
)

app_custom_openapi = FastAPI()

app.include_router(licenselibrary.router)
app.include_router(dependencygraph.router)
app.include_router(compatibilitycheck.router)
app.include_router(datasets.router)

app_custom_openapi.include_router(licenselibrary.router)
app_custom_openapi.include_router(dependencygraph.router)
app_custom_openapi.include_router(compatibilitycheck.router)
app_custom_openapi.include_router(datasets.router)

def custom_openapi():
    if app.openapi_schema:
        return app.openapi_schema
    openapi_schema = get_openapi(
        title="License API",
        version="1.1 Coypu adoption of DALICC",
        description="Enhanced Data Licenses Clearance Center API",
        routes=app_custom_openapi.routes,
    )
    app.openapi_schema = openapi_schema
    return app.openapi_schema


app.openapi = custom_openapi
