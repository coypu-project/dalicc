from fastapi import APIRouter, Body, Header, HTTPException, Response
from typing import Optional, Any
from pydantic import BaseModel
from enum import Enum
from SPARQLWrapper import SPARQLWrapper, JSON
from rdflib import Graph
import simplejson as json
from typing import Any, Dict, AnyStr, List, Union
from urllib.parse import urlparse
import random
import string

JSONObject = Dict[AnyStr, Any]
JSONArray = List[Any]
JSONStructure = Union[JSONArray, JSONObject]


router = APIRouter(
    prefix="/licenselibrary",
    tags=["licenselibrary"],
    responses={404: {"description": "Not found"}},
)
sparql = SPARQLWrapper("http://virtuoso-db:8890/sparql")


class ActionState(Enum):
    permitted = 'permitted'
    na = 'na'
    prohibited = 'prohibited'
    notProhibited = 'notProhibited'


class DutyState(Enum):
    required = 'required'
    na = 'na'


class TargetState(Enum):
    true = 'yes'
    false = 'no'


class TargetSearch(BaseModel):
    creativework: TargetState = TargetState.true
    dataset: TargetState = TargetState.true
    software: TargetState = TargetState.true


class ActionsSearch(BaseModel):
    reproduce: ActionState = ActionState.na
    distribute: ActionState = ActionState.na
    modify: ActionState = ActionState.na
    derive: ActionState = ActionState.na
    commercial_use: ActionState = ActionState.na
    charge_distribution_fee: ActionState = ActionState.na
    change_license: ActionState = ActionState.na
    transform: ActionState = ActionState.na
    aggregate: ActionState = ActionState.na
    extract: ActionState = ActionState.na


class DutiesSearch(BaseModel):
    distribute_duty_attribution: DutyState = DutyState.na
    distribute_duty_notice: DutyState = DutyState.na
    distribute_duty_source_code: DutyState = DutyState.na
    modify_duty_rename: DutyState = DutyState.na
    modify_duty_attribution: DutyState = DutyState.na
    modify_duty_modification_notice: DutyState = DutyState.na
    modify_duty_notice: DutyState = DutyState.na
    modify_duty_source_code: DutyState = DutyState.na
    derive_duty_rename: DutyState = DutyState.na
    derive_duty_attribution: DutyState = DutyState.na
    derive_duty_modification_notice: DutyState = DutyState.na
    derive_duty_notice: DutyState = DutyState.na
    derive_duty_source_code: DutyState = DutyState.na
    change_license_duty_compliant_license = DutyState.na


class LicenseWideDutiesSearch(BaseModel):
    share_alike: DutyState = DutyState.na


class LicenseSearch(BaseModel):
    target: TargetSearch
    actions: ActionsSearch
    duties: DutiesSearch
    license_wide_duties: LicenseWideDutiesSearch


@router.get("/license/{license_id}")
def get_license_by_id(license_id, short = False, Accept: str = Header('application/ld+json')):
    """
    Get the machine-readable representation of the license from the DALICC license library or a human readable HTML representation.
    
    The range of the license URIs can be listed via the **licenselibrary/list** endpoint.

    Parameters:
    * _license\_id_: (type: _string_) 
        * E.g.: **Apache-2.0**, **0BSD**, **MIT**, **MS-PL**, **CC-BY-NC-SA-4.0**, **CC-BY-3.0-NL**, **CC-BY-4.0**

    * Headers: <- does not work in Swagger
        * Accept: Either application/ld+json or text/html
    """

    print("Accept: ", Accept)

    if Accept != "application/ld+json" and not Accept.startswith("text/html"):
        raise HTTPException(status_code=406, detail="Not Acceptable")

    if "application/ld+json" in Accept:
        g = Graph()
        try:
            g.parse("licensedata/licenses/"+license_id+".ttl", format="ttl")
        except:
            pass
        response = json.loads(g.serialize(format="json-ld"))
        return Response(content=json.dumps(response), media_type="application/ld+json")

    else:
        texts = loadTexts(sparql)
        def getValue(obj, key, get_last_path_from_url_per_Item = False, onlyTheFirst = False):
            values = set()
            iterable = obj['results']['bindings']
            for item in iterable:
                for key2 in item.keys():
                    if (key == key2):
                        if get_last_path_from_url_per_Item:
                            values.add(get_last_path_from_url(item[key]['value']))
                        else:
                            values.add(item[key]['value'])

            if onlyTheFirst:
                return values.pop()
            return ', '.join(values)
            
        def getActions(iterable, type):
            """
            Go through SPARQL result and gather the actions of type 'type'.
            Then create the HTML part for them.
            """

            html = '<div class="license-permissions-data"><ul>'
            #entry = '<div><ul><li>'+get_last_path_from_url(item["o"]["value"])+'</li></ul>'
            my_dict = {
                "no-duties": [],
                "with-duties": []
            }
            for item in iterable:
                if get_last_path_from_url(item["p"]["value"]) == type:
                    duties = item["duty_actions"]["value"]
                    if duties == "":
                        my_dict["no-duties"].append(item["o"]["value"])
                    else:
                        duties_array = duties.split(", ")
                        duties = [s for s in duties_array]
                        my_dict["with-duties"].append({
                            "action": item["o"]["value"],
                            "duties": duties
                        })
            for action in my_dict["no-duties"]:
                html += f'<li>{getHmlElement(action, texts)}</li>'
            html += '</ul></div>'
            for item in my_dict["with-duties"]:
                html += f'<div class="license-permissions-data"><ul><li>{getHmlElement(item["action"], texts)}</li></ul><div class="license-duties"><h5>Duties:</h5></div><ul>'
                for duty in item["duties"]:
                    html += f'<li>{getHmlElement(duty, texts)}</li>'
                html += f'</ul></div>'
            return html
                    
        new_query = """
        	PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
            PREFIX odrl:	<http://www.w3.org/ns/odrl/2/>
            PREFIX dct:	<http://purl.org/dc/terms/>
            PREFIX cc:	<http://creativecommons.org/ns#>
            PREFIX dalicc:	<http://dalicc.net/ns#>
            SELECT DISTINCT ?s ?target ?title ?juristication ?type ?source ?publisher ?liabilityLimitation ?warrantyDisclaimer ?additionalClauses
            WHERE
            {
              {
                ?s odrl:target ?coll ;
                    dct:title ?title ;
                    cc:jurisdiction ?juristication .
                ?coll dct:type ?target .
                optional {?s dalicc:validityType ?type .}
                optional {?s dct:publisher ?publisher .}
                optional {?s dct:source ?source .}
                optional {?s dalicc:LiabilityLimitation ?liabilityLimitation .}
                optional {?s dalicc:WarrantyDisclaimer ?warrantyDisclaimer .}
                optional {?s dalicc:additionalClauses ?additionalClauses .}
                FILTER (?s in (<http://dalicc.net/licenselibrary/""" + license_id + """>, <https://metadata.coypu.org/licenses/""" + license_id + """>)) .
              }
              UNION
              {
                ?s rdfs:seeAlso ?s2 .
                ?s2 odrl:target ?coll ;
                    dct:title ?title ;
                    cc:jurisdiction ?juristication .
                optional { ?coll dct:type ?target . }
                optional { ?s odrl:target ?coll2 . ?coll2 dct:type ?target }
                optional {?s2 dalicc:validityType ?type .}
                optional {?s2 dct:publisher ?publisher .}
                optional {?s2 dct:source ?source .}
                optional {?s2 dalicc:LiabilityLimitation ?liabilityLimitation .}
                optional {?s2 dalicc:WarrantyDisclaimer ?warrantyDisclaimer .}
                optional {?s2 dalicc:additionalClauses ?additionalClauses .}
                FILTER (?s = <https://metadata.coypu.org/licenses/""" + license_id + """>) .
              }
            }
        	"""
        print("::: Gather general license information:\n", new_query)
        sparql.setReturnFormat(JSON)
        sparql.setQuery(new_query)
        license = sparql.query().convert()
        print("::: Results:\n", license['results']['bindings'])

        if short:
            return Response(content=shortHTML(license_id), media_type="text/html")

        language = '"en"'
        actions_query = """
            PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
            PREFIX odrl:	<http://www.w3.org/ns/odrl/2/>
            prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>
            prefix skos: <http://www.w3.org/2004/02/skos/core#>
            prefix dct: <http://purl.org/dc/terms/>

            Select distinct (?p_name as ?p) (?o as ?o) ?duty_actions
            WHERE {
                        {SELECT DISTINCT ?s ?p ?o (GROUP_CONCAT(?duty_action; separator=", ") as ?duty_actions)
                        WHERE
                        {
                            {
                                ?s ?p ?o1 .
                                ?o1 odrl:action ?o .
                                FILTER (?s in (<http://dalicc.net/licenselibrary/""" + license_id + """>, <https://metadata.coypu.org/licenses/""" + license_id + """>)) .
                                FILTER (?p IN (odrl:permission, odrl:prohibition, odrl:obligation, odrl:duty) ).
                                optional { ?o1 odrl:duty ?duty .
                                    ?duty odrl:action ?duty_action . }
                            }
                            UNION
                            {
                                ?s rdfs:seeAlso ?s2 .
                                ?s2 ?p ?o1 .
                                ?o1 odrl:action ?o .
                                FILTER (?s = <https://metadata.coypu.org/licenses/""" + license_id + """>) .
                                FILTER (?p IN (odrl:permission, odrl:prohibition, odrl:obligation, odrl:duty) ).
                                optional { ?o1 odrl:duty ?duty .
                                    ?duty odrl:action ?duty_action . }
                            }
                        }}
                        
                        OPTIONAL {
                        ?s rdfs:label ?s_name1 .
                        FILTER ( lang(?s_name1) =""" + language + """)
                        }
                        OPTIONAL {
                        ?s skos:prefLabel ?s_name2 .
                        FILTER ( lang(?s_name2) =""" + language + """)
                        }
                        OPTIONAL {
                        ?s dct:title ?s_name3 .
                        FILTER ( lang(?s_name3) =""" + language + """)
                        }
                        BIND(coalesce(?s_name1, ?s_name2, ?s_name3, ?s) as ?s_name)

                        OPTIONAL {
                        ?p rdfs:label ?p_name1 .
                        FILTER ( lang(?p_name1) =""" + language + """)
                        }
                        OPTIONAL {
                        ?p skos:prefLabel ?p_name2 .
                        FILTER ( lang(?p_name2) =""" + language + """)
                        }
                        OPTIONAL {
                        ?p dct:title ?p_name3 .
                        FILTER ( lang(?p_name3) =""" + language + """)
                        }
                        bind(coalesce(?p_name1, ?p_name2, ?p_name3, ?p) as ?p_name)
            }
            """
        print("::: Gather detailed license information:\n", actions_query)
        sparql.setReturnFormat(JSON)
        sparql.setQuery(actions_query)
        actions = sparql.query().convert()
        print("::: Results:\n", actions['results']['bindings'])

        with open('app/html/license.css', 'r', encoding='utf-8') as file:
            css = file.read()
        html = '''
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Coypu - License details</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <style>
        '''+css+'''
    </style>
</head>
<body class="html">
    <div class="container">
        <h3>''' + str(getValue(license, "title")) + '''</h3>
        <ul>
            <li>
                <strong>Target</strong>: ''' + str(getValue(license, "target", True)) + '''
            </li>
            <li>
                <strong>Region</strong>: ''' + getHmlElement(str(getValue(license, "juristication")), texts) + '''
            </li>
            <li>
                <strong>Validity period</strong>: ''' + getHmlElement(str(getValue(license, "type")), texts) + '''
            </li>
            <li>
                <strong>Source</strong>: <a href="''' + str(getValue(license, "source")) + '''" target=”_blank”>''' + str(getValue(license, "source")) + '''</a>
            </li>
            <li>
                <strong>License Owner</strong>: ''' + str(getValue(license, "publisher")) + '''
            </li>
            <li>
                <strong>''' + getHmlElement("http://creativecommons.org/ns#ShareAlike", texts) + '''</strong>: ''' + str(isLicenseShareAlike(sparql, license_id)) + '''
            </li>
        </ul>
        <table>
            <tr>
                <th>Permissions</th>
                <th>Prohibitions</th>
            </tr>
            <tr>
                <td style="vertical-align: top;">''' + getActions(actions['results']['bindings'], "Has Permission") + '''</td>
                <td style="vertical-align: top; border-left: 2px dotted #7c9297;">''' + getActions(actions['results']['bindings'], "Has Prohibition") + '''</td>
            </tr>
        </table>
        <pre>Hint: The permission and prohibitions could also have constraints. For details have a look at the <a href="https://metadata.coypu.org/?''' + getValue(license, "s", False, True) + '''">source RDF</a>.</pre>
        <br>
        <strong>Liability</strong>: ''' + str(getValue(license, "liabilityLimitation")) + '''<br><br>
        <strong>Warranty</strong>: ''' + str(getValue(license, "warrantyDisclaimer")) + '''<br><br>
        <strong>Additional clauses</strong>: ''' + str(getValue(license, "additionalClauses")) + '''<br><br>
    </div>
</body>
</html>
            '''

        return Response(content=html, media_type="text/html")


@router.get("/list")
def list_licenses_in_the_license_library(keyword: Optional[str] = None, skip: Optional[int] = 0, limit: Optional[int] = 10):
    """
    Get the list of the licenses (_URI_ and _title_) stored in the license library.

    *  Parameters:
        * _keyword_: (type: _string_) Title of the license contains _keyword_.
        * _skip_: (type: _int_) Offsets the results by given value for paging.
        * _limit_: (type: _int_) Limits the number of returned results.
    """
    q = keyword
    if q:
        q_part_t = "FILTER CONTAINS(LCASE(?title),\""+str(q).lower()+"\").\n"
        q_part_a = "FILTER CONTAINS(LCASE(?title_alternative),\"" + \
            str(q).lower()+"\").\n"
    else:
        q_part_t = ""
        q_part_a = ""

    query = """
     PREFIX dct: <http://purl.org/dc/terms/> 
        PREFIX odrl: <http://www.w3.org/ns/odrl/2/> 
        PREFIX dcmitype: <http://purl.org/dc/dcmitype/> 
        PREFIX dalicc: <http://dalicc.net/ns#> 
        PREFIX cc: <http://creativecommons.org/ns#> 
        SELECT DISTINCT ?id ?title FROM <http://dalicc.net/licenselibrary/> 
        WHERE {
            {
                ?id rdf:type odrl:Set.
                ?id dct:title ?title.\n"""+q_part_t+"""
            }
            UNION
            {
                ?id rdf:type odrl:Set.
                ?id dct:title ?title.
                ?id dct:alternative ?title_alternative.\n"""+q_part_a+"""}
            }
        ORDER BY ASC(?title)
        LIMIT """+str(limit)+"""
        OFFSET """+str(skip)

    sparql.setQuery(query)
    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()

    return(results)


@router.post("/facetedsearch")
def faceted_search(facets: LicenseSearch) -> dict:
    """
    Search for licenses that satisfy certain criteria.

    Request body:
    * request[**"target"**][...]: (type: _TargetState_) **"yes" / "no"**
    * request[**"actions"**][...]: (type: _ActionState_) **"na" / "permitted" / "prohibited" / "notProhibited"**
    * request[**"duties"**][...]: (type: _DutyState_) **"na" / "required"**
    * request[**"license_wide_duties"**][...]: (type: _DutyState_) **"na" / "required"**
    """
    return facetedSearchLicenses(facets)
    

def facetedSearchLicenses(facets, datasets=False):
    print("got facets: ", facets)
    duty_map = {"attribution": "cc:Attribution",
                "notice": "cc:Notice",
                "source_code": "cc:SourceCode",
                "rename": "dalicc:rename",
                "modification_notice": "dalicc:modificationNotice",
                "compliant_license": "dalicc:compliantLicense",
                "share_alike": "cc:ShareAlike"}

    action_map = {"reproduce": "odrl:reproduce",
                  "distribute": "odrl:distribute",
                  "modify": "odrl:modify",
                  "derive": "odrl:derive",
                  "commercial_use": "cc:CommercialUse",
                  "charge_distribution_fee": "dalicc:chargeDistributionFee",
                  "change_license": "dalicc:ChangeLicense",
                  "share_alike": "cc:ShareAlike",
                  "transform": "odrl:transform",
                  "aggregate": "odrl:aggregate",
                  "extract": "odrl:extract"}

    p_map = action_map.copy()

    for kd in duty_map:
        for ka in action_map:
            p_map[ka+"_duty_"+kd] = duty_map[kd]

    prefixes = """PREFIX dct: <http://purl.org/dc/terms/>
PREFIX odrl: <http://www.w3.org/ns/odrl/2/>
PREFIX dcmitype: <http://purl.org/dc/dcmitype/>
PREFIX dalicc: <http://dalicc.net/ns#>
PREFIX cc: <http://creativecommons.org/ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
"""
    
    no_action_selected = True
    for p, value in facets.actions:
        if value != ActionState.na:
            no_action_selected = False

    in_clause = "("

    provenance_clause = "?id dct:type "
    provenance_query_clause = ""
    provenance_not_exist_clause = ""

    for c, target in [("dalicc:CreativeWork", facets.target.creativework), ("dcmitype:Dataset", facets.target.dataset), ("dcmitype:Software", facets.target.software)]:
        if target == TargetState.true:
            in_clause = in_clause + c + ","
            provenance_query_clause = provenance_query_clause + provenance_clause + c + ".\n"
        if target == TargetState.false:
            provenance_not_exist_clause += "FILTER NOT EXISTS{\n ?asset dct:type " + c + ".\n}\n"

    in_clause = in_clause[:-1]
    in_clause += ")"
    in_clause = "FILTER (?assettype IN " + in_clause + ")."
    if (facets.target.creativework == facets.target.dataset == facets.target.software == TargetState.false):
        in_clause = ""
    if datasets:
        in_clause = ""
        provenance_not_exist_clause = ""

    if no_action_selected:
        sa_addition = ""
        if facets.license_wide_duties.share_alike == DutyState.required:
            p_cur_id = 1000
            d_cur_id = 1001
            sa_addition += "?id odrl:duty "+"?d" + \
                str(d_cur_id)+". \n ?d"+str(d_cur_id) + \
                " odrl:action "+p_map["share_alike"]+".\n"

        query = """SELECT DISTINCT ?id ?title 
                WHERE
                {   {
                        ?id a odrl:Set ;
                            odrl:target ?asset.
                        ?id dct:title ?title.
                        ?asset dct:type ?assettype.
                        """+sa_addition+"""
                    }
                    """+in_clause+"""
                    """+provenance_not_exist_clause+"""
                }"""
        if datasets:
            query = """SELECT DISTINCT ?id ?title 
                WHERE {
                {   {
                        ?id a odrl:Set ;
                              rdfs:seeAlso ?id2 .
                        ?id2 dct:title ?title.
                        """+sa_addition.replace("?id ", "?id2 ")+"""
                    }
                    optional { ?id odrl:target ?asset.
                        ?asset dct:type ?assettype. }
                    optional { ?id2 odrl:target ?asset.
                        ?asset dct:type ?assettype. }
                } union {
                    {
                        ?id a odrl:Set ;
                            odrl:target ?asset.
                        ?id dct:title ?title.
                        ?asset dct:type ?assettype.
                        """+sa_addition+"""
                    }
                }}"""
        print("::: facetedSearchLicenses query:\n", prefixes+query)
        sparql.setQuery(prefixes+query)
        sparql.setReturnFormat(JSON)
        results = sparql.query().convert()
        return results

    permission_once = True
    prohibition_once = True

    query = "SELECT DISTINCT ?id ?title \nWHERE\n{\n{{ ?id a odrl:Set ;"

    p_set = set(map(str, range(1, 15)))
    d_set = set(map(str, range(1, 150)))
    subquery = " dct:title ?title.\n"
    for p, value in facets.actions:
        p_cur_id = p_set.pop()

        if value == ActionState.permitted:

            subquery += "?id odrl:permission ?p" + \
                str(p_cur_id)+". \n ?p"+str(p_cur_id) + \
                " odrl:action "+p_map[p]+".\n"
            for d, value_d in facets.duties:
                d_cur_id = d_set.pop()
                if p in d and value_d == DutyState.required:
                    subquery += "?p"+str(p_cur_id)+" odrl:duty "+"?d"+str(d_cur_id) + \
                        ". \n ?d"+str(d_cur_id)+" odrl:action "+p_map[d]+".\n"
            if permission_once:
                subquery += "?id"+" odrl:target ?asset.\n ?asset dct:type ?assettype.\n"
                permission_once = False

        elif value == ActionState.prohibited:
            subquery += "?id odrl:prohibition ?p" + \
                str(p_cur_id)+". \n ?p"+str(p_cur_id) + \
                " odrl:action "+p_map[p]+".\n"
            if prohibition_once:
                subquery += "?id"+" odrl:target ?asset.\n ?asset dct:type ?assettype.\n"
                prohibition_once = False
        elif value == ActionState.notProhibited:
            subquery += "FILTER NOT EXISTS {\n?id odrl:prohibition ?p" + \
                str(p_cur_id)+".\n ?p"+str(p_cur_id) + \
                " odrl:action "+p_map[p]+".\n}"

    if facets.license_wide_duties.share_alike == DutyState.required:
        d_cur_id = d_set.pop()
        subquery += "?id odrl:duty "+"?d" + \
            str(d_cur_id)+". \n ?d"+str(d_cur_id) + \
            " odrl:action "+p_map["share_alike"]+".\n"

    subquery += "}"+in_clause+"\n"+provenance_not_exist_clause
    query += subquery+"\n} union {\n?id a odrl:Set ; rdfs:seeAlso ?id2 . optional { ?id odrl:target ?asset. ?asset dct:type ?assettype. } optional { ?id2 odrl:target ?asset. ?asset dct:type ?assettype. } ?id2"
    query += subquery.replace("?id ", "?id2 ").replace("?id2 odrl:target ?asset.\n ?asset dct:type ?assettype.", " ")+"""}"""

    print("::: facetedSearchLicenses query:\n", prefixes+query)
    sparql.setQuery(prefixes+query)
    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()
    return results

def isAValidJSON(input_json):
    try:
        input_json[b'userID']
    except KeyError:
        return False, "Error: User ID is missing"
    return True, None

def isLicenseCopyleft(identifier, permissions, prohibitions):
    """
    The correct logic is unclear. The definition is soft. There could be multiple levels of copyleft but this is uncommon.
    """

    if identifier in ["BSD-2", "BSD-3", "BSD-4", "MIT", "Apache-1.1", "Apache-2.0"]:
        return False
    if "dalicc:ChangeLicense" in prohibitions and "odrl:modify" in permissions and "odrl:distribute" in permissions and "odrl:use" in permissions: #odrl:use different for odrl:target
        return True

def isLicenseShareAlike(sparql, licenseName):
    """
    Share-Alike is only true if the duty is there at the root.
    It could also be thought about checking the prohibitions and permissions. E.g. Share-Alike could be prohibited or permitted
    """
    
    query = '''
        prefix : <https://metadata.coypu.org/licenses/>
        prefix odrl: <http://www.w3.org/ns/odrl/2/>
        prefix cc: <http://creativecommons.org/ns#>
        prefix dalicclib: <http://dalicc.net/licenselibrary/>

        ASK WHERE {
        
            ?iri
            odrl:duty [ 
                a odrl:Duty ;
                odrl:action cc:ShareAlike 
            ].
            filter(?iri in (<http://dalicc.net/licenselibrary/'''+licenseName+'''>, <http://dalicc.net/licenselibrary/'''+licenseName+'''>))
        }
    '''
    print("::: isLicenseShareAlike ASK\n", query)
    sparql.setReturnFormat(JSON)
    sparql.setQuery(query)
    result = sparql.query().convert()
    # print(result)

    return result["boolean"]

def loadTexts(sparql):
    """
    Does load the labels and description of the schemas CC, ODRL, DALICC and our own
    """

    query = """
        prefix : <https://metadata.coypu.org/licenses/>
        prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        prefix odrl:	<http://www.w3.org/ns/odrl/2/>
        prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        prefix skos: <http://www.w3.org/2004/02/skos/core#>
        prefix dct: <http://purl.org/dc/terms/>
        prefix cc: <http://creativecommons.org/ns#>
        prefix dalicclib: <http://dalicc.net/licenselibrary/>

        Select distinct ?p ?p_label ?p_def
        where {
        ?p rdfs:label ?p_label .
        FILTER ( lang(?p_label) = "en")
        optional { ?p skos:definition ?p_def . }

        filter( strStarts(str(?p), "http://creativecommons.org/ns") or strStarts(str(?p), "http://www.w3.org/ns/odrl/2") or strStarts(str(?p), "http://dalicc.net/ns") or strStarts(str(?p), "https://metadata.coypu.org/licenses/schema") )
        }
        """
    
    sparql.setReturnFormat(JSON)
    sparql.setQuery(query)
    result = sparql.query().convert()
    print("::: loadTexts result: ", len(result['results']['bindings']), " entries")

    return result['results']['bindings']

def getHmlElement(iri, texts, withScript = True):
    """
    This function returns a string which contains an HTML element using the related label and description of the given iri.
    """
    letters = string.ascii_letters + string.digits  # alphanumeric characters
    id = ''.join(random.choice(letters) for i in range(12))

    label = ""
    description = ""
    for item in texts:
        if iri == item["p"]["value"]:
            # print("::: Found the label in the texts:\n", item)
            label = item["p_label"]["value"]
            try:
                description = item["p_def"]["value"]
            except Exception as e:
                description += label
                label = ""

    if label == "":
        label = get_last_path_from_url(iri)

    # print("::: getHmlElement: id=", id, " label=", label, "   iri was: ", iri)

    element = f"""<div id="{id}" class="item">
        <span class="name">{label}</span>
        <span class="tooltip-icon" data-tooltip="">?</span>
    </div>"""
    if withScript:
        element += f"""
            <script>
                document.getElementById("{id}").querySelector(".tooltip-icon").addEventListener("mouseover", function() {{
                    this.dataset.tooltip = "{description}";
                }});
            </script>
        """

    return element


def get_last_path_from_url(url):
    try:
        # check if url is not IRI
        if not ("http" in url and "/" in url):
            return url

        parsed = urlparse(url)
        if parsed.fragment:
            return parsed.fragment.split('/')[-1]
        else:
            return parsed.path.split('/')[-1]
    except Exception as e:
        # log exception details if any, useful for debugging
        print(f"Exception occurred: {e}")
        return url
    

def shortHTML(license_id):
    texts = loadTexts(sparql)
    def getValue(obj, key, get_last_path_from_url_per_Item = False, onlyTheFirst = False):
        values = set()
        iterable = obj['results']['bindings']
        for item in iterable:
            for key2 in item.keys():
                if (key == key2):
                    if get_last_path_from_url_per_Item:
                        values.add(get_last_path_from_url(item[key]['value']))
                    else:
                        values.add(item[key]['value'])

        if onlyTheFirst:
            return values.pop()
        return ', '.join(values)
                
    new_query = """
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        PREFIX odrl:	<http://www.w3.org/ns/odrl/2/>
        PREFIX dct:	<http://purl.org/dc/terms/>
        PREFIX cc:	<http://creativecommons.org/ns#>
        PREFIX dalicc:	<http://dalicc.net/ns#>
        SELECT DISTINCT ?s ?target ?title ?juristication ?type ?source ?publisher ?liabilityLimitation ?warrantyDisclaimer ?additionalClauses
        WHERE
        {
            {
            ?s odrl:target ?coll ;
                dct:title ?title ;
                cc:jurisdiction ?juristication .
            ?coll dct:type ?target .
            optional {?s dalicc:validityType ?type .}
            optional {?s dct:publisher ?publisher .}
            optional {?s dct:source ?source .}
            optional {?s dalicc:LiabilityLimitation ?liabilityLimitation .}
            optional {?s dalicc:WarrantyDisclaimer ?warrantyDisclaimer .}
            optional {?s dalicc:additionalClauses ?additionalClauses .}
            FILTER (?s in (<http://dalicc.net/licenselibrary/""" + license_id + """>, <https://metadata.coypu.org/licenses/""" + license_id + """>)) .
            }
            UNION
            {
            ?s rdfs:seeAlso ?s2 .
            ?s2 odrl:target ?coll ;
                dct:title ?title ;
                cc:jurisdiction ?juristication .
            optional { ?coll dct:type ?target . }
            optional { ?s odrl:target ?coll2 . ?coll2 dct:type ?target }
            optional {?s2 dalicc:validityType ?type .}
            optional {?s2 dct:publisher ?publisher .}
            optional {?s2 dct:source ?source .}
            optional {?s2 dalicc:LiabilityLimitation ?liabilityLimitation .}
            optional {?s2 dalicc:WarrantyDisclaimer ?warrantyDisclaimer .}
            optional {?s2 dalicc:additionalClauses ?additionalClauses .}
            FILTER (?s = <https://metadata.coypu.org/licenses/""" + license_id + """>) .
            }
        }
        """
    print("::: Gather general license information:\n", new_query)
    sparql.setReturnFormat(JSON)
    sparql.setQuery(new_query)
    license = sparql.query().convert()
    print("::: Results:\n", license['results']['bindings'])

    html = '''
<div class="container">
    <h3>''' + str(getValue(license, "title")) + '''</h3>
    <ul>
        <li>
            <strong>Target</strong>: ''' + str(getValue(license, "target", True)) + '''
        </li>
        <li>
            <strong>Region</strong>: ''' + getHmlElement(str(getValue(license, "juristication")), texts, False) + '''
        </li>
        <li>
            <strong>Validity period</strong>: ''' + getHmlElement(str(getValue(license, "type")), texts, False) + '''
        </li>
        <li>
            <strong>Source</strong>: <a href="''' + str(getValue(license, "source")) + '''" target=”_blank”>''' + str(getValue(license, "source")) + '''</a>
        </li>
        <li>
            <strong>License Owner</strong>: ''' + str(getValue(license, "publisher")) + '''
        </li>
        <li>
            <strong>''' + getHmlElement("http://creativecommons.org/ns#ShareAlike", texts, False) + '''</strong>: ''' + str(isLicenseShareAlike(sparql, license_id)) + '''
        </li>
    </ul>
    <pre>This is just a basic overview. For details have a look at the <a href="https://licenseapi.coypu.org/licenselibrary/license/''' + license_id + '''" target=”_blank”>HTMl representation</a>.</pre>
</div>
    '''

    return html