from fastapi import APIRouter, Response
from SPARQLWrapper import SPARQLWrapper, JSON, BASIC
from typing import Any, Dict, AnyStr, List, Union, TypeVar
import os
from pydantic import BaseModel
import sys
sys.path.append('/app/app/routers/') # without this line the file could not be found
from compatibilitycheck import getCompatibilityWithDetails
from licenselibrary import LicenseSearch, TargetSearch, TargetState, ActionsSearch, DutiesSearch, LicenseWideDutiesSearch
from licenselibrary import facetedSearchLicenses, shortHTML, loadTexts, get_last_path_from_url, getHmlElement
import math

class LicensesJSON(BaseModel):
    licenses: list = []

class DatasetsJSON(BaseModel):
    datasets: list = []

class LicenseSearch2(BaseModel):
    actions: ActionsSearch
    duties: DutiesSearch
    license_wide_duties: LicenseWideDutiesSearch

router = APIRouter(
    prefix="/datasets",
    tags=["datasets"],
    responses={404: {"description": "Not found"}},
)
sparql_local = SPARQLWrapper("http://virtuoso-db:8890/sparql")
sparql_remote = SPARQLWrapper("https://skynet.coypu.org/coypu-internal/")
sparql_remote.setHTTPAuth(BASIC)

@router.get("/{dataset_id}/citation")
def citation(dataset_id):
    """
    Get the HTML snipped containing most relevant information for citing.
    If you cite, add what you did with the dataset. Like modified or derived.

    Parameters:
    * _dataset\_id_: (type: _string_)
        * E.g.: **baci**
    """

    def get_value(results, key):
        try:
            return results["results"]["bindings"][0][key]["value"]
        except Exception:
            return ""

    query = """
        PREFIX dct: <http://purl.org/dc/terms/>
        PREFIX dcat:  <http://www.w3.org/ns/dcat#>
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        SELECT * WHERE { Graph <https://metadata.coypu.org/> {
          ?dataset a <https://schema.coypu.org/metadata-template#Dataset> ;
          	dct:identifier """+'"'+dataset_id+'"'+""" ;
            <https://schema.coypu.org/metadata-template#theme> ?theme .
            optional { ?dataset <https://schema.coypu.org/metadata-template#licenseNote> ?note }
            optional { ?dataset <https://schema.coypu.org/metadata-template#licensePage> ?licensePage }
            optional { ?dataset dct:license ?license }
            optional { ?dataset dct:creator ?creator }
            optional { ?dataset dct:description ?name }
            optional { ?dataset dcat:landingPage ?landingPage }
            optional {
				?dataset dcat:distribution ?dist .
          		?dist dct:modified ?date .
            }
          }} """
    print("::: query: ", query)

    sparql_remote.setCredentials(os.getenv('SPARQL_REMOTE_USERNAME'), os.getenv('SPARQL_REMOTE_PASSWORD'))
    sparql_remote.setQuery(query)
    sparql_remote.setReturnFormat(JSON)
    results = sparql_remote.query().convert()

    licenseURI = get_value(results, "license")
    print("::: license: ", licenseURI)

    note = get_value(results, "note")
    landingPage = get_value(results, "landingPage")
    licensePage = get_value(results, "licensePage")
    theme = get_value(results, "theme")
    creator = get_value(results, "creator")
    date = get_value(results, "date")
    name = get_value(results, "name")

    nameAndAuthor = '<a href="' + landingPage + '">' + name + "</a> by " + creator
    if name == "" or creator == "":
        nameAndAuthor = '<a href="' + landingPage + '">' + dataset_id + '</a>: about ' + theme

    if date != "":
        nameAndAuthor += " (" + date + ")"

    if licensePage != "":
        note = '<a href="' + licensePage + '">' + note + "</a>"

    if licenseURI == "":
        return nameAndAuthor + ', this dataset is licensed under '  + note


    query2 = """
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        PREFIX odrl:	<http://www.w3.org/ns/odrl/2/>
        SELECT DISTINCT *
        WHERE
        {
          {
            ?s <http://purl.org/dc/terms/title> ?title .
            optional { ?s <http://purl.org/dc/terms/source> ?source }
               optional { ?s <http://creativecommons.org/ns#legalcode> ?legalcode }
            FILTER (?s = <""" + licenseURI + """>) .
          }
          UNION
          {
            ?s rdfs:seeAlso ?s2 .
            ?s2 <http://purl.org/dc/terms/title> ?title .
            optional { ?s2 <http://purl.org/dc/terms/source> ?source }
            optional { ?s2 <http://creativecommons.org/ns#legalcode> ?legalcode }
            FILTER (?s = <""" + licenseURI + """>) .
          }
        }
        """
    print("::: query2: ", query2)

    sparql_local.setQuery(query2)
    sparql_local.setReturnFormat(JSON)
    results2 = sparql_local.query().convert()
    #print("::: results2: ", results2)

    licensePage = get_value(results2, "source") or get_value(results2, "legalcode") or licenseURI
    longLicenseName = get_value(results2, "title") or "There is an error with the linked license " + licenseURI

    text = nameAndAuthor + ', this dataset is licensed under <a href="' + licensePage + '">' +  longLicenseName + '</a>'

    return Response(content=text, media_type="text/html")

@router.post("/facetedsearch")
def faceted_search(facets: LicenseSearch2) -> dict:
    """
    Search for datasets that satisfy certain license criteria.

    Request body:
    * request[**"actions"**][...]: (type: _ActionState_) **"na" / "permitted" / "prohibited" / "notProhibited"**
    * request[**"duties"**][...]: (type: _DutyState_) **"na" / "required"**
    * request[**"license_wide_duties"**][...]: (type: _DutyState_) **"na" / "required"**
    """
    ls = LicenseSearch(
        target=TargetSearch(
            creativework=TargetState.false,
            dataset=TargetState.true,
            software=TargetState.false
        ),
        actions=facets.actions,
        duties=facets.duties,
        license_wide_duties=facets.license_wide_duties
    )
    # Use license search to get a list of relevant licenses
    results = facetedSearchLicenses(ls, True)
    licenses = set()
    for o in results["results"]["bindings"]:
        licenses.add(o["id"]["value"])

    # Before using these licenses, the to datasets linked licenses have to be read
    query = """
        PREFIX dct: <http://purl.org/dc/terms/>
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        SELECT distinct ?license WHERE { Graph <https://metadata.coypu.org/> {
            ?dataset a <https://schema.coypu.org/metadata-template#Dataset> ;
                dct:license ?license ;
                <https://schema.coypu.org/metadata-template#type> ?type .
    		OPTIONAL { ?dataset a <https://schema.coypu.org/metadata-template#Dataset> ;
                dct:license ?license2 .
      			?license2 rdfs:seeAlso ?license . }
            Filter(?type not in ("Ontology"))
          }} """
    print("::: query for reading all linked licenses: ", query)
    sparql_remote.setCredentials(os.getenv('SPARQL_REMOTE_USERNAME'), os.getenv('SPARQL_REMOTE_PASSWORD'))
    sparql_remote.setQuery(query)
    sparql_remote.setReturnFormat(JSON)
    licenses_results = sparql_remote.query().convert()
    print("::: number of linked licenses: ", len(licenses_results["results"]["bindings"]))
    # Remove not linked licenses
    filteredLicenses = set()
    for licenseObj in licenses_results["results"]["bindings"]:
        if licenseObj["license"]["value"] in licenses:
            filteredLicenses.add(licenseObj["license"]["value"])
        else:
            print("license ", licenseObj["license"]["value"], " gets flitered out.")
    print("::: From ", len(licenses), " licenses, only ", len(filteredLicenses), " are relevant.")

    iris = ">, <".join(filteredLicenses)
    query = """
        PREFIX dct: <http://purl.org/dc/terms/>
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        SELECT * WHERE { Graph <https://metadata.coypu.org/> {
            ?dataset a <https://schema.coypu.org/metadata-template#Dataset> ;
          	    dct:identifier ?identifier ;
                dct:license ?license ;
                <https://schema.coypu.org/metadata-template#type> ?type .
            optional { ?dataset <http://www.w3.org/ns/dcat#landingPage> ?landingPage .
                ?dataset <https://schema.coypu.org/metadata-template#graph> ?graph .
                ?dataset <https://schema.coypu.org/metadata-template#theme> ?theme .
            }
            FILTER (?license in (<""" + iris + """>))
            Filter(?type not in ("Ontology"))
          }} order by ?identifier """
    print("::: query for get datasets: ", query)

    sparql_remote.setQuery(query)
    sparql_remote.setReturnFormat(JSON)
    datasets_results = sparql_remote.query().convert()
    print("::: number of datasets: ", len(datasets_results["results"]["bindings"]))

    return datasets_results

@router.post("/aggregates/")
def aggregates(input_json: DatasetsJSON, commercialUse = False, aIUsage = False
    ):
    """
    This route does take a list of datasets and return an HTML website showing all relevant legal aspects regarding the datasets.
    It's  based on their licenses, the terms of use and conflicts.
    Of importance are information like: commercial use, ShareAlike

    Later there could be additional parameters for including legal advise regarding the kind of dashboard used.

    Request body:
    * request["datasets"][...]: (type: _datasetIRI_)
        * E.g. 1: {"datasets": ["https://metadata.coypu.org/dataset/baci", "https://metadata.coypu.org/dataset/climatetrace"]}
    """

    # Overview of tasks
    """
    read dataset information and fill SECTION_A + DETAILS_A

    read license information and fill SECTION_B + DETAILS_B

    load conflicts and fill SECTION_C and SECTION_D plus DETAILS_C +  decide about SECTION_X

    fill SECTION_E+SECTION_F+DETAILS_E

    decide about DETAILS_G
    """

    section_numbers = {
        'SECTION_A': len(input_json.datasets),
        'SECTION_B': 1,
        'SECTION_C': 0,
        'SECTION_D': 0,
        'SECTION_E': 0,
        'SECTION_F': 0,
        'SECTION_X': 'The constellation of licenses could be used in the dashboard without issues.'
    }

    overlay_details = {
        'DETAILS_A': '',
        'DETAILS_B': '',
        'DETAILS_C': '',
        'DETAILS_E': '',
        'DETAILS_G': ''
    }

    # Get information about datasets like licenses
    iris = ">, <".join(input_json.datasets)
    query = """
        PREFIX dct: <http://purl.org/dc/terms/>
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        SELECT * WHERE { Graph <https://metadata.coypu.org/> {
            ?dataset a <https://schema.coypu.org/metadata-template#Dataset> ;
          	    dct:identifier ?identifier .
            optional { ?dataset dct:license ?license . }
            optional { ?dataset <http://purl.org/dc/terms/description> ?description . }
            FILTER (?dataset in (<""" + iris + """>)) .
          }} """
    print("::: query for getting licenses of datasets: ", query)
    sparql_remote.setCredentials(os.getenv('SPARQL_REMOTE_USERNAME'), os.getenv('SPARQL_REMOTE_PASSWORD'))
    sparql_remote.setQuery(query)
    sparql_remote.setReturnFormat(JSON)
    datasets_results = sparql_remote.query().convert()

    # fill DETAILS_A
    overlay_details["DETAILS_A"] = """
        <h3>Dataset IRIs</h3>
        <p>Click on one to open a new tab with information about it.</p>
        <br>
    """
    for dataset in input_json.datasets:
        name = "dataset"
        for ds in datasets_results["results"]["bindings"]:
            if ds["dataset"]["value"] == dataset:
                if "description" in ds:
                    name = ds["description"]["value"]
                else:
                    name = ds["identifier"]["value"]
                
        overlay_details["DETAILS_A"] += f"""
            <a href="{dataset}" target=”_blank”>{name} </a>
            <br>

        """

    licenses_ = set()
    for item in datasets_results["results"]["bindings"]:
        licenses_.add(item["license"]["value"])
    licenses = list(filter(None, licenses_))
    print("::: licenses: ", licenses)

    # fill SECTION_B and DETAILS_B
    section_numbers["SECTION_B"] = len(licenses)
    for license in licenses:
        datasetName = ""
        for ds in datasets_results["results"]["bindings"]:
            if ds["license"]["value"] == license:
                if "description" in ds:
                    datasetName = ds["description"]["value"]
                else:
                    datasetName = ds["identifier"]["value"]
        if datasetName != "":
            overlay_details["DETAILS_B"] += f"<b>{datasetName}:</b>"
        overlay_details["DETAILS_B"] += shortHTML(get_last_path_from_url(license))

    # ask reasoner for conflicts (or directly the existing function which does return turtle)
    licenses_json = LicensesJSON(licenses=licenses)
    jsonld = getCompatibilityWithDetails(licenses_json)

    # fill SECTION_C, SECTION_D
    countDirect = 0
    countIndirect = 0
    for iri in jsonld["@graph"]:
        for conflict in iri["dalicc-e:hasConflict"]:
            if conflict["dalicc-e:type"] == "direct":
                countDirect += 1
            else:
                countIndirect += 1
    section_numbers["SECTION_C"] = math.floor(countDirect) # divide through number of licenses?
    section_numbers["SECTION_D"] = math.floor(countIndirect)

    # Prepare more information
    # get license data
    licensesQuery = """
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        PREFIX odrl:	<http://www.w3.org/ns/odrl/2/>
        PREFIX dct:	<http://purl.org/dc/terms/>
        PREFIX cc:	<http://creativecommons.org/ns#>
        PREFIX dalicc:	<http://dalicc.net/ns#>
        SELECT DISTINCT ?s ?title ?juristication ?type ?source ?publisher ?prohibition ?permission ?duty
        WHERE
        {
            {
            ?s dct:title ?title ;
                cc:jurisdiction ?juristication .
            optional {?s dalicc:validityType ?type .}
            optional {?s dct:publisher ?publisher .}
            optional {?s dct:source ?source .}
            FILTER (?s in (<""" + ">, <".join(licenses) + """>)) .
    		optional {?s odrl:prohibition ?blank_node1 . ?blank_node1 odrl:action ?prohibition .}
    		optional {?s odrl:permission ?blank_node2 . ?blank_node2 odrl:action ?permission .}
    		optional {?s odrl:duty ?blank_node3 . ?blank_node3 odrl:action ?duty .}
            }
            UNION
            {
            ?s rdfs:seeAlso ?s2 .
            ?s2 dct:title ?title ;
                cc:jurisdiction ?juristication .
            optional {?s2 dalicc:validityType ?type .}
            optional {?s2 dct:publisher ?publisher .}
            optional {?s2 dct:source ?source .}
            FILTER (?s in (<""" + ">, <".join(licenses) + """>)) .
    		optional {?s2 odrl:prohibition ?blank_node1 . ?blank_node1 odrl:action ?prohibition .}
    		optional {?s2 odrl:permission ?blank_node2 . ?blank_node2 odrl:action ?permission .}
    		optional {?s2 odrl:duty ?blank_node3 . ?blank_node3 odrl:action ?duty .}
            }
        }
        """
    print("::: query for get license details: ", licensesQuery)
    sparql_local.setQuery(licensesQuery)
    sparql_local.setReturnFormat(JSON)
    rdf_licenses = sparql_local.query().convert()
    # print("::: rdf_licenses: ", rdf_licenses)
    # Go through the results and enhance the jsonld
    for item in rdf_licenses["results"]["bindings"]:
        # print("::: qitem: ", item["s"]["value"])
        license = next((x for x in jsonld["@graph"] if x["@id"] == item["s"]["value"]), None)
        # print("::: license: ", license)
        if license == None:
            break;
        license["title"] = item["title"]["value"]
        license["juristication"] = item["juristication"]["value"]
        if "prohibitions" not in license:
                license["prohibitions"] = set()
        if "type" in item:
            license["validityType"] = item["type"]["value"]
        if "source" in item:
            license["source"] = item["source"]["value"]
        if "publisher" in item:
            license["publisher"] = item["publisher"]["value"]

        if "duty" in item and item["duty"]["value"] != None:
            if "duties" not in license:
                license["duties"] = set()
            license["duties"].add(item["duty"]["value"])
        if "permission" in item and item["permission"]["value"] != None:
            if "permissions" not in license:
                license["permissions"] = set() # not valid JSON but is only used as dictionary
            license["permissions"].add(item["permission"]["value"])
        if "prohibition" in item and item["prohibition"]["value"] != None:
            license["prohibitions"].add(item["prohibition"]["value"])
    # print("::: jsonld: ", jsonld)


    # fill DETAILS_C
    texts = loadTexts(sparql_local)
    def useName(iri):
        label = ""
        for item in texts:
            if iri == item["p"]["value"]:
                label = item["p_label"]["value"]

        if label == "":
            label = get_last_path_from_url(iri)
        return label
    
    temp_allConflicts = []
    for iri in jsonld["@graph"]:
        for conflict in iri["dalicc-e:hasConflict"]:
            temp_allConflicts.append(conflict)
    
    overlay_details["DETAILS_C"] += "<b>Direct conflicts:</b><br>"
    for conflict in temp_allConflicts:
        # print("::: iri object: ", iri)
        if conflict["dalicc-e:type"] == "direct":
            overlay_details["DETAILS_C"] += iri["title"] + " has the " + ("<b>" + conflict["dalicc-e:type"] + "</b>") + " conflict with " + get_last_path_from_url(conflict["dalicc-e:otherLicense"]["@id"]) + ":<br>"
            overlay_details["DETAILS_C"] += "The " + get_last_path_from_url(conflict["dalicc-e:ownPredicate"]["@id"]) + " \"" + useName(conflict["dalicc-e:ownObject"]["@id"]) + "\" is conflicting with " + get_last_path_from_url(conflict["dalicc-e:otherPredcate"]["@id"]) + " \"" + useName(conflict["dalicc-e:otherObject"]["@id"]) + "\"<br><br>"
        
    overlay_details["DETAILS_C"] += "<b>Indirect conflicts:</b><br>"
    for conflict in temp_allConflicts:
        # print("::: iri object: ", iri)
        if conflict["dalicc-e:type"] != "direct":
            overlay_details["DETAILS_C"] += iri["title"] + " has the " + conflict["dalicc-e:type"] + " conflict with " + get_last_path_from_url(conflict["dalicc-e:otherLicense"]["@id"]) + ":<br>"
            overlay_details["DETAILS_C"] += "The " + get_last_path_from_url(conflict["dalicc-e:ownPredicate"]["@id"]) + " \"" + useName(conflict["dalicc-e:ownObject"]["@id"]) + "\" is conflicting with " + get_last_path_from_url(conflict["dalicc-e:otherPredcate"]["@id"]) + " \"" + useName(conflict["dalicc-e:otherObject"]["@id"]) + "\"<br><br>"


    # fill SECTION_E+SECTION_F+DETAILS_E
    # First prepare sets
    resulting_permissions = set()
    all_prohibitions = set()
    for iri in jsonld["@graph"]:
        if len(resulting_permissions) < 1:
            for permission in iri["permissions"]:
                resulting_permissions.add(permission)
        else:
            temp = set()
            for permission in iri["permissions"]:
                temp.add(permission)
            resulting_permissions = resulting_permissions.intersection(temp)
        if "prohibitions" in iri: 
            for prohibition in iri["prohibitions"]:
                all_prohibitions.add(prohibition)
    section_numbers["SECTION_E"] = len(resulting_permissions)
    section_numbers["SECTION_F"] = len(all_prohibitions)
    # Now create details
    permissions = '<div class="license-permissions-data"><ul>'
    for permission in resulting_permissions:
        permissions += '<li>'+getHmlElement(permission, texts, False).replace('<span class="tooltip-icon" data-tooltip="">?</span>', '')+'</li>'
    permissions += '</ul></div>'
    prohibitions = '<div class="license-permissions-data"><ul>'
    for prohibition in all_prohibitions:
        prohibitions += '<li>'+getHmlElement(prohibition, texts, False).replace('<span class="tooltip-icon" data-tooltip="">?</span>', '')+'</li>'
    prohibitions += '</ul></div>'
    
    overlay_details["DETAILS_E"] = f"""
        <table>
            <tr>
                <th>Permissions</th>
                <th>Prohibitions</th>
            </tr>
            <tr>
                <td style="vertical-align: top;">{permissions}</td>
                <td style="vertical-align: top; border-left: 2px dotted #7c9297;">{prohibitions}</td>
            </tr>
        </table>
        <br>
        <br>
        <b>Hint:</b> These are the adjusted permissions and prohibitions.
        <br>That means that only permissions are listed which are not proibited and are given in each license.
        <br>The list of prohibitions is a summary of all prohibitions.
    """

    # Decide about X (summary text)
    # TODO: also handle the both flags
    changedSummary = False
    if countDirect > 0:
        section_numbers["SECTION_X"] = "There is a major conflict between the licenses. Have a look at the details of the conflicts. This dataset constellation cannot be used."
        changedSummary = True
    if not reduce(canLicenseBeUsedForGeneralProcessing, jsonld["@graph"], True):
        if changedSummary:
            section_numbers["SECTION_X"] += "<br>At least one license does not allow the usage in a dashboard. Please have a look at the licenses to identify the dataset(s). This dataset constellation cannot be used."
        else:
            section_numbers["SECTION_X"] += "At least one license does not allow the usage in a dashboard. Please have a look at the licenses to identify the dataset(s). This dataset constellation cannot be used."
        changedSummary = True
    if not reduce(canLicenseBeUsedForCombination, jsonld["@graph"], True):
        if changedSummary:
            section_numbers["SECTION_X"] += "<br>At least one license does not allow the combination with other datasets. Please have a look at the licenses to identify the dataset(s). This dataset constellation cannot be used."
        else:
            section_numbers["SECTION_X"] += "At least one license does not allow the combination with other datasets. Please have a look at the licenses to identify the dataset(s). This dataset constellation cannot be used."

    # Fill DETAILS_G
    overlay_details["DETAILS_G"] = """
        The following hints are not complete and are there for highlighting aspects which are relevant in the context of the Coypu project.
        <br><br>
    """
    overlay_details["DETAILS_G"] += """
        <b>The datasets are combined thus the following aspects have to be considered:</b>
        <br>
        <i>
            For dataset combination the licenses have to be compatible. After the combination, all licenses of the origin datasets are still valid. Thus the terms of use of them have to be respected. That means that respecting the rights of copyright owners, like naming the copyright owners, still has to be done. Also other duties are still valid.
            <br>
            When combining datasets, DSGVO-relevant aspects are important. There could be a need to clean or anonymize the data!
        </i>
    """
    if commercialUse:
        overlay_details["DETAILS_G"] += """
            <br>
            <br>
            <b>Commercial use:</b>
            <br>
            <i>
                This aspect is restrictive, thus if the dataset owner is contacted and agrees upon, commercial could be possible even when there is a conflict regarding commercial use of a dataset.
            </i>
        """
    if aIUsage:
        overlay_details["DETAILS_G"] += """
            <br>
            <br>
            <b>AI (Use the dataset for machine learning or LLM training):</b>
            <br>
            <i>
                The duration for which data may be stored for machine learning and LLM (Language Model Learning) training varies depending on whether the training is conducted within the private sector or by a scientific institute. Additionally, the accessibility to this data also holds significance.
                <br>
                Another aspect to consider is the licensing agreement: if it stipulates a change in licensing terms, the data generated using the corresponding dataset must comply with these revised terms.
            </i>
        """



    with open('app/html/license.css', 'r', encoding='utf-8') as file:
            css = file.read()
    finalResult = loadHTMLAggregate(section_numbers, overlay_details, css)
    return Response(content=finalResult, media_type="text/html")
    
    


    # build object
    conflictsExisting = False
    commercialUse = False
    shareAlike = False
    abortReason = ""
    abort = reduce(canLicenseBeUsedForGeneralProcessing, jsonld["@graph"], True)
    if abort:
        abortReason = "At least one license does prohibit derivate work."


    # is missing: cc:jurisdiction, dalicc:LiabilityLimitation, dalicc:WarrantyDisclaimer, dalicc:additionalClauses
    # As it should analyse the aggragate, the resulting prohibitions, permissions and duties are relevant.
    # Assumption is that prohibitions do delete the permissions.
    # Some odrl:Actions as prohibitions do result in a No to processing the aggregate.
    # Also what is about actions which are not permitted and not prohibited? Not allowed
    analysis = {
        "generalIssue": abortReason,
        "possibleToCombine": conflictsExisting == False, # more checks are neccessary
        "conflictsExisting": conflictsExisting,
        "commercialUse": commercialUse,
        "shareAlike": shareAlike,
        "duties": [],
        "permission": [],
        "prohibitions": []
    }

    return analysis

def reduce(function, iterable, initializer=None):
    it = iter(iterable)
    if initializer is None:
        value = next(it)
    else:
        value = initializer
    for element in it:
        value = function(value, element)
    return value

def canLicenseBeUsedForGeneralProcessing(v, e):
    """
    Function is used to check if the permissions and prohibitions do allow a general processing.
    """
    # TODO: Change it so that permissions have to exist
    return (v
        and not any(prohibition == "http://creativecommons.org/ns#DerivativeWorks" for prohibition in e["prohibitions"])
        and not any(prohibition == "http://www.w3.org/ns/odrl/2/derive" for prohibition in e["prohibitions"])
        and not any(prohibition == "http://dalicc.net/ns#addStatement" for prohibition in e["prohibitions"]))

def canLicenseBeUsedForCombination(v, e):
    """
    Function is used to check if the permissions and prohibitions do allow a combination with other datasets.
    """
    return (v
        and any(permission == "http://www.w3.org/ns/odrl/2/distribute" for permission in e["permissions"])
        and any(permission == "http://www.w3.org/ns/odrl/2/derive" for permission in e["permissions"])
        and not any(prohibition == "http://www.w3.org/ns/odrl/2/transform" for prohibition in e["prohibitions"])
        and not any(prohibition == "http://www.w3.org/ns/odrl/2/aggregate" for prohibition in e["prohibitions"])
        and not any(prohibition == "http://www.w3.org/ns/odrl/2/extract" for prohibition in e["prohibitions"]))

def loadHTMLAggregate(section_numbers, overlay_details, css):
    # Define the path to your HTML template file
    template_file_path = 'app/html/licensing_information.html'

    # Read the HTML template from the file
    with open(template_file_path, 'r', encoding='utf-8') as file:
        html_template = file.read()

    # Replace the placeholders with actual content
    for placeholder, value in section_numbers.items():
        html_template = html_template.replace('{{' + placeholder + '}}', str(value))

    for placeholder, content in overlay_details.items():
        html_template = html_template.replace('{{' + placeholder + '}}', content)

    html_template = html_template.replace('{{EXTRA_CSS}}', css)

    # The final HTML is now stored in a variable
    return html_template