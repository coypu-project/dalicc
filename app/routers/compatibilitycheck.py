from fastapi import APIRouter, Header, HTTPException
from typing import Optional, Any
from pydantic import BaseModel
from SPARQLWrapper import SPARQLWrapper, JSON
from rdflib import Graph
import json
from typing import Any, Dict, AnyStr, List, Union
import requests

JSONObject = Dict[AnyStr, Any]
JSONArray = List[Any]
JSONStructure = Union[JSONArray, JSONObject]

class LicensesJSON(BaseModel):
    licenses: list = []

def isAValidJSON(input_json):
    try:
        input_json[b'userID']
    except KeyError:
        print(input_json)
        return False,"Error: User ID is missing"
    return True,None


router = APIRouter(
    prefix="/compatibilitycheck",
    tags=["compatibilitycheck"],
    responses={404: {"description": "Not found"}},
)
sparql = SPARQLWrapper("http://virtuoso-db:8890/sparql")
reasoner_address = "http://dalicc-reasoner:80/reasoner/compatibility"

@router.post("/")
def compatibility(input_json: LicensesJSON,
    Content_Type: Optional[str] = Header(default='application/json'),
    Accept: Optional[str] = Header(default='text/turtle')
    ):
    """
    Compatibility check of multiple (2 or more) licenses that returns the conflicting statements in JSON format.
    Note: the curl command is not working corectly - replace the headers  with CamelCase spelling and set the values you actually want

    Request body:
    * request["licenses"][...]: (type: _licenseURI_)
        * E.g. 1: {"licenses": ["http://dalicc.net/licenselibrary/StatisticsCanadaOpenLicenceAgreement","http://dalicc.net/licenselibrary/UkOpenGovernmentLicenseForPublicSectorInformation"]}
        * E.g. 2: {"licenses": ["https://metadata.coypu.org/licenses/Cc010Universal", "https://metadata.coypu.org/licenses/CC-BY-NC-SA_v4"]}
        * E.g. 3: {"licenses": ["https://metadata.coypu.org/licenses/DLDEBY20", "https://metadata.coypu.org/licenses/Weltrisikobericht"]}

    * Headers:
        * Content-Type: application/json
        * Accept: Either application/json or text/turtle
    """

    if Content_Type not in ["application/json"]:
        raise HTTPException(status_code=415, detail="Unsupported Media Type")

    if Accept not in ["application/json", "text/turtle"]:
        raise HTTPException(status_code=406, detail="Not Acceptable")

    if "application/json" in Accept:
        response = requests.post(reasoner_address,  data = input_json.json())
        return response.json()
    else:
        jsonld = getCompatibilityWithDetails(input_json)
        # print(jsonld)
        g = Graph()
        g.parse(data=json.dumps(jsonld), format="json-ld")
        return g.serialize(format="ttl")

def getCompatibilityWithDetails(input_json):
    """
    This function runs the compatibilitycheck, fills a JSON-LD and then adds more information to it.
    """

    # Prepare objects
    reasoner_return = requests.post(reasoner_address,  data = input_json.json())
    """
    Example of jsonls:
        * {
            "@context": {
            "odrl": "http://www.w3.org/ns/odrl/2/",
            "label": "http://www.w3.org/2000/01/rdf-schema#label",
            "dalicc-e": "http://dalicc.net/licenselibrary/enhancement/"
            },
            "@graph": [
            {
                "@id": "http://dalicc.net/licenselibrary/CC-BY-v4",
                "dalicc-e:hasConflict": [
                {
                    "@id": "http://dalicc.net/licenselibrary/conflict1",
                    "label": "Direct permission-prohibition conflict.",
                    "dalicc-e:type": "direct",
                    "dalicc-e:ownPredicate": {"@id": "http://www.w3.org/ns/odrl/2/permission"},
                    "dalicc-e:ownObject": {"@id": "http://dalicc.net/ns#ChangeLicense"},
                    "dalicc-e:otherLicense": {"@id": "http://dalicc.net/licenselibrary/CreativeCommonsAttributionSharealike20Germany"},
                    "dalicc-e:otherPredcate": {"@id": "http://www.w3.org/ns/odrl/2/prohibition"},
                    "dalicc-e:otherObject": {"@id": "http://dalicc.net/ns#ChangeLicense"}
                }
                ]
            }
            ]
        }
    """
    jsonld = {
        "@context": {
            "odrl": "http://www.w3.org/ns/odrl/2/",
            "label": "http://www.w3.org/2000/01/rdf-schema#label",
            "dalicc-e": "http://dalicc.net/licenselibrary/enhancement/"
        },
        "@graph": []
    }
    conflictCounter = 0

    #define functions
    def createIRIJsonld(v):
        return {
            "@id": v,
            "@type": "@id"
        }

    def useItem(item, myType, myLicense, conflictCounter):
        if item["statement_1"][0] != myLicense and item["statement_2"][0] != myLicense:
            return

        licensedictExisting = False
        for licenseDict in jsonld["@graph"]:
            if licenseDict["@id"] == myLicense:
                licensedictExisting = True

        subJsonld = {
            "@id": "http://dalicc.net/licenselibrary/conflict"+str(conflictCounter),
            "@type": "dalicc-e:Conflict",
            "label": item["reason"],
            "dalicc-e:type": myType
        }

        if item["statement_1"][0] == myLicense:
            subJsonld["dalicc-e:ownPredicate"] = createIRIJsonld(item["statement_1"][1])
            subJsonld["dalicc-e:ownObject"] = createIRIJsonld(item["statement_1"][2])
        else:
            subJsonld["dalicc-e:otherLicense"] = createIRIJsonld(item["statement_1"][0])
            subJsonld["dalicc-e:otherPredcate"] = createIRIJsonld(item["statement_1"][1])
            subJsonld["dalicc-e:otherObject"] = createIRIJsonld(item["statement_1"][2])
        if item["statement_2"][0] == myLicense:
            subJsonld["dalicc-e:ownPredicate"] = createIRIJsonld(item["statement_2"][1])
            subJsonld["dalicc-e:ownObject"] = createIRIJsonld(item["statement_2"][2])
        else:
            subJsonld["dalicc-e:otherLicense"] = createIRIJsonld(item["statement_2"][0])
            subJsonld["dalicc-e:otherPredcate"] = createIRIJsonld(item["statement_2"][1])
            subJsonld["dalicc-e:otherObject"] = createIRIJsonld(item["statement_2"][2])

        if item["statement_1"][0] == myLicense and item["statement_2"][0] == myLicense:
            subJsonld["dalicc-e:ownPredicate"] = createIRIJsonld(item["statement_1"][1])
            subJsonld["dalicc-e:ownObject"] = createIRIJsonld(item["statement_1"][2])
            subJsonld["dalicc-e:otherLicense"] = createIRIJsonld(item["statement_2"][0])
            subJsonld["dalicc-e:otherPredcate"] = createIRIJsonld(item["statement_2"][1])
            subJsonld["dalicc-e:otherObject"] = createIRIJsonld(item["statement_2"][2])

        if licensedictExisting == False:
            licenseDict = {
                "@id": myLicense,
                "@type": "odrl:Set",
                "dalicc-e:hasConflict": [subJsonld]
            }
            jsonld["@graph"].append(licenseDict)
        else:
            index = -1
            #print(":::useItem::: trying to add |||", subJsonld, "||| to ", jsonld)
            counter = 0
            for value in jsonld["@graph"]:
                if value["@id"] == myLicense:
                    index = counter
                counter += 1
            if index > -1:
                jsonld["@graph"][index]["dalicc-e:hasConflict"].append(subJsonld)

    # Load JSON as dictionary
    dict = reasoner_return.json() # conflicting_statements is the main key
    #print("::: Dict from reasoner: ", dict)

    # build new json with the prefered structure
    # First get all licenses
    licenses = set(input_json.licenses)
    if "direct" in dict["conflicting_statements"]:
        for key, item in dict["conflicting_statements"]["direct"].items():
            licenses.add(item["statement_1"][0])
            licenses.add(item["statement_2"][0])
    if "derived" in dict["conflicting_statements"]:
        for key, item in dict["conflicting_statements"]["derived"].items():
            licenses.add(item["statement_1"][0])
            licenses.add(item["statement_2"][0])
    print("::: Licenses: ", licenses)

    # For each license, search where it is named and add something to the jsonld
    for license in licenses:
        if "direct" in dict["conflicting_statements"]:
            for key, item in dict["conflicting_statements"]["direct"].items():
                conflictCounter += 1
                useItem(item, "direct", license, conflictCounter)
        if "derived" in dict["conflicting_statements"]:
            for  key, item in dict["conflicting_statements"]["derived"].items():
                conflictCounter += 1
                useItem(item, "derived", license, conflictCounter)

    #print("::: Dict before parsing to ttl: ", jsonld)

    # Now prepare data to show which licenses are for commercial use
    query = """
        PREFIX cc: <http://creativecommons.org/ns#>
        PREFIX odrl: <http://www.w3.org/ns/odrl/2/>
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        PREFIX dalicc-e: <http://dalicc.net/licenselibrary/enhancement/>

        select distinct ?license ?type
        WHERE {
        {
            ?license a odrl:Set ;
            ?link ?rule .

            ?rule a ?type ;
                odrl:action cc:CommercialUse .

            filter (( ?link = odrl:prohibition || ?link = odrl:permission ) && ?license in (<$licenses>) )
        }
        UNION
        {
            ?license rdfs:seeAlso ?s .
            ?s a odrl:Set ;
            ?link ?rule .

            ?rule a ?type ;
                odrl:action cc:CommercialUse .

            filter (( ?link = odrl:prohibition || ?link = odrl:permission ) && ?license in (<$licenses>) )
        }

        }
        order by asc(?license)
    """
    query = query.replace("$licenses", ">, <".join(licenses))
    sparql.setQuery(query)
    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()
    licensesAndRuletype = results["results"]["bindings"]
    print("::: licensesAndRuletype: ", licensesAndRuletype)
    # loop through and set info per license
    for element in licensesAndRuletype:
        license = element["license"]["value"]
        type = element["type"]["value"].split("/")[-1]
        licenseObject = [x for x in jsonld["@graph"] if x["@id"] == license][0]
        index = jsonld["@graph"].index(licenseObject)
        # print("::: type: ", type)
        # print("::: index: ", index)
        jsonld["@graph"][index]["dalicc-e:commercialUse"] = True if type == "Permission" else False
        # Detect if a license is share-alike
        jsonld["@graph"][index]["dalicc-e:shareAlike"] = isLicenseShareAlike(sparql, jsonld["@graph"][index]["@id"])

    return jsonld

def isLicenseShareAlike(sparql, iri):
    """
    Share-Alike is only true if the duty is there at the root.
    It could also be thought about checking the prohibitions and permissions. E.g. Share-Alike could be prohibited or permitted
    """
    
    query = '''
        prefix : <https://metadata.coypu.org/licenses/>
        prefix odrl: <http://www.w3.org/ns/odrl/2/>
        prefix cc: <http://creativecommons.org/ns#>
        prefix dalicclib: <http://dalicc.net/licenselibrary/>

        ASK WHERE {
            {
                <''' + iri + '''>
                odrl:duty [ 
                    a odrl:Duty ;
                    odrl:action cc:ShareAlike 
                ].
            }
            UNION
            {
                <''' + iri + '''> rdfs:seeAlso ?s2 .
                ?s2 odrl:duty [ 
                    a odrl:Duty ;
                    odrl:action cc:ShareAlike 
                ].
            }
        }
    '''
    # print(query)
    sparql.setReturnFormat(JSON)
    sparql.setQuery(query)
    result = sparql.query().convert()
    # print(result)

    return result["boolean"]
