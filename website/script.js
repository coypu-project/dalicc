document.getElementById('switch-form').addEventListener('submit', function(e) {
    e.preventDefault();

    var flags = [];
    for (var i = 1; i <= document.querySelectorAll('input[type="radio"]').length/2; i++) {
        var value = document.querySelector('input[name="switch'+i+'"]:checked').value;
        if (value !== "")
            flags.push(value);
    }

    var requestBody = {
        actions: {
            reproduce: "na",
            distribute: flags.indexOf("co") > -1 ? "permitted" : "na",
            modify: "na",
            derive: flags.indexOf("ai") > -1 || flags.indexOf("co") > -1 ? "permitted" : "na",
            commercial_use: flags.indexOf("cu") > -1 ? "permitted" : "na",
            charge_distribution_fee: "na",
            change_license: "na",
            transform: flags.indexOf("co") > -1 ? "notProhibited" : "na",
            aggregate: flags.indexOf("co") > -1 ? "notProhibited" : "na",
            extract: flags.indexOf("co") > -1 ? "notProhibited" : "na"
        },
        duties: {
            distribute_duty_attribution: "na",
            distribute_duty_notice: "na",
            distribute_duty_source_code: "na",
            modify_duty_rename: "na",
            modify_duty_attribution: "na",
            modify_duty_modification_notice: "na",
            modify_duty_notice: "na",
            modify_duty_source_code: "na",
            derive_duty_rename: "na",
            derive_duty_attribution: "na",
            derive_duty_modification_notice: "na",
            derive_duty_notice: "na",
            derive_duty_source_code: "na",
            change_license_duty_compliant_license: "na"
        },
        license_wide_duties: {
            share_alike: "na"
        }
    };

    var url = 'http://localhost:8002/datasets/facetedsearch'
    if (!location.host.startsWith("localhost"))
        url = 'https://licenseapi.coypu.org/datasets/facetedsearch'
    fetch(url, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(requestBody),
    })
    .then(response => response.json())
    .then(data => {
        var resultsList = document.getElementById('results-list');
        resultsList.innerHTML = '';
        data.results.bindings.forEach(result => {
            var listItem = document.createElement('li');
            listItem.className = 'list-group-item';
            listItem.innerHTML = `
                <div class="result-content">
                    <h3><a href="${result.dataset.value}" target=”_blank”>${result.identifier.value}</a></h3> 
                    <p>${result.theme?.value ?? result.dataset.value}, stored in graph ${result.graph?.value ?? '<not yet set>'}. Under license <a href="${result.license.value}" target=”_blank”>${result.license.value}</a> with the landing page: <a href="${result.landingPage?.value ?? ''}" target=”_blank”>${result.landingPage?.value}</a></p>
                </div>
            `;
            listItem.dataset.url = result.dataset.value;
            resultsList.appendChild(listItem);
        });

        // Scrollt zur Höhe der Liste
        resultsList.scrollIntoView({behavior: "smooth"});
    })
    .catch((error) => {
        console.error('Error:', error);
        alert("There was an error. Contact Kurt (InfAI) if it is prevalent.")
    });
});