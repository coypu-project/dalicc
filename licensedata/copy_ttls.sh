#!/bin/bash

mkdir -p ../virtuoso_data/ttl_dump
cd licensedata
cp dependencygraph/dg_default.ttl ../virtuoso_data/ttl_dump/dg_default.ttl
cp dependencygraph/dg_default.ttl.graph ../virtuoso_data/ttl_dump/dg_default.ttl.graph
cp licenselibrary/licenselibrary.ttl ../virtuoso_data/ttl_dump/licenselibrary.ttl
cp licenselibrary/licenselibrary.ttl.graph ../virtuoso_data/ttl_dump/licenselibrary.ttl.graph
cp schema/CCschema.ttl ../virtuoso_data/ttl_dump/CCschema.ttl
cp schema/CCschema.ttl.graph ../virtuoso_data/ttl_dump/CCschema.ttl.graph
cp schema/ODRL22.ttl ../virtuoso_data/ttl_dump/ODRL22.ttl
cp schema/ODRL22.ttl.graph ../virtuoso_data/ttl_dump/ODRL22.ttl.graph
cp schema/schema.ttl ../virtuoso_data/ttl_dump/schema.ttl
cp schema/schema.ttl.graph ../virtuoso_data/ttl_dump/schema.ttl.graph
cd ..
